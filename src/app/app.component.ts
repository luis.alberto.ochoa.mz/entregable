import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public title: string = 'Entregable';

  public contador: number = 0;


  public sumar(value: number): void {
    this.contador += value;
  }
}
